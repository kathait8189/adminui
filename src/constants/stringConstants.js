export const stringConstants = {
    passwordChangedMsg: 'Password update successfully!',
    deleteMsg: 'Record deleted successfully!',
    recordAddedMsg: 'Record added successfully!',
    recordUpdatedMsg: 'Record updated successfully!',
    unsavedFormLeaveHead: 'Leaving form confirmation!',
    unsavedFormLeave: 'You have unsaved information, are you sure you want to leave this page?'
};
