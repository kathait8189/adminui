import loginModel from './login';
import comonModel from './comon';
import homeModel from './home';
import userModel from './user';
import pagesModel from './pages';

const storeModel = {
  login: loginModel,
  comon: comonModel,
  home: homeModel,
  user: userModel,
  pages: pagesModel
};

export default storeModel;
