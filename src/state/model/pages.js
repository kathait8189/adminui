import { action, thunk } from 'easy-peasy';
import * as pagesService from '../../services/pages';

const pagesModel = {
    pagesList: [],
    setPagesList: action((state, payload) => {
        state.pagesList = payload;
    }),
    getPagesList: thunk(async (actions, payload) => {
        await pagesService.getPagesList(payload)
        .then(function (response) {
            actions.setPagesList(response.data.data.list);
        })
        .catch(function (error) {
            actions.setPagesList([]);
        });
    }),
    page: {},
    setPage: action((state, payload) => {
        state.page = payload;
    }),
    getPages: thunk(async (actions, payload) => {
        await pagesService
        .getPages(payload)
        .then(function (response) {
            actions.setPage(response.data);
        })
        .catch(function (error) {
            console.log('Error', error);
            actions.setUser({});
        });
    }),
    addPages: thunk(async (actions, payload) => {
        return await pagesService.addPages(payload);
    }),
    updatePages: thunk(async (actions, payload) => {
        return await pagesService.updatePages(payload);
    }),
    deletePages: thunk(async (actions, payload) => {
        return await pagesService.deletePages(payload);
    })
};

export default pagesModel;
