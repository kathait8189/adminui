import { action, thunk } from 'easy-peasy';
import * as usersService from '../../services/user';

const userModel = {
    userList: [],
    setUserList: action((state, payload) => {
        state.userList = payload;
    }),
    getUserList: thunk(async (actions, payload) => {
        await usersService.getUserList(payload)
        .then(function (response) {
            actions.setUserList(response.data.data.list);
        })
        .catch(function (error) {
            actions.setUserList([]);
        });
    }),
    user: {},
    setUser: action((state, payload) => {
        state.user = payload;
    }),
    getUser: thunk(async (actions, payload) => {
        await usersService
        .getUser(payload)
        .then(function (response) {
            actions.setUser(response.data);
        })
        .catch(function (error) {
            console.log('Error', error);
            actions.setUser({});
        });
    }),
    addUser: thunk(async (actions, payload) => {
        return await usersService.addUser(payload);
    }),
    updateUser: thunk(async (actions, payload) => {
        return await usersService.updateUser(payload);
    }),
    deleteUser: thunk(async (actions, payload) => {
        return await usersService.deleteUser(payload);
    })
};

export default userModel;
