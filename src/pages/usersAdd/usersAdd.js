import React from 'react';
import {
  makeStyles,
  Grid,
  Typography,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  UserFormFields
} from '../../components';
import { useStoreState, useStoreActions } from 'easy-peasy';

/** STYLES HERE * */
const useStyles = makeStyles(() => ({
  
}));

const UsersAddPage = (props) => {
  const classes = useStyles();
  const userAction = useStoreActions((actions) => actions.user);
  const userState = useStoreState((state) => state.user);
  const [pageType, setPageType] = React.useState('add');
  const [expandAddUser, setExpandAddUser] = React.useState(true);

  const getDetails = (id) => {
    try {
      userAction.getUser(id);
    } catch (err) {
      console.log("Error", err);
    }
  };

  React.useEffect(() => {
    if (props.match && props.match.params.id) {
      if (props.location && props.location.search.indexOf('edit=true') !== -1) {
        setPageType('edit');
      } else {
        setPageType('view');
      }
      getDetails(props.match.params.id);
    }
  }, []);

  return (
    <Grid container>
      <Grid item xs={12} md={12} lg={12}>      
        <ExpansionPanel
          expanded={expandAddUser}
          onChange={(e, exp) => setExpandAddUser(!expandAddUser)}
        >
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant="h6" style={{ textTransform: 'capitalize' }}>
              {pageType} User
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <UserFormFields
              history={props.history}
              pageType={pageType}
              setPageType={setPageType}
              formValues={userState.user}
              cancelClick={() => props.history.push('/users')}
            />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Grid>
    </Grid>
  );
};

export default UsersAddPage;
