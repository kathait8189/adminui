import React from 'react';
import {
  makeStyles,
  Grid,
  Typography,
  Button,
  ExpansionPanel,
  ExpansionPanelSummary,
  ExpansionPanelDetails
} from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import {
  DynamicPageFormFields
} from '../../components';
import { useStoreState, useStoreActions } from 'easy-peasy';

/** STYLES HERE * */
const useStyles = makeStyles(() => ({
  
}));

const DynamicPageAdd = (props) => {
  const classes = useStyles();
  const pagesAction = useStoreActions((actions) => actions.pages);
  const pagesState = useStoreState((state) => state.pages);
  const [pageType, setPageType] = React.useState('add');
  const [expandAddPolicy, setExpandAddPolicy] = React.useState(true);

  const getDetails = (id) => {
    try {
      pagesAction.getPages(id);
    } catch (err) {
      console.log("Error", err);
    }
  };

  React.useEffect(() => {
    if (props.match && props.match.params.id) {
      if (props.location && props.location.search.indexOf('edit=true') !== -1) {
        setPageType('edit');
      } else {
        setPageType('view');
      }
      getDetails(props.match.params.id);
    }
  }, []);

  return (
    <Grid container>
      <Grid item xs={12} md={12} lg={12}>      
        <ExpansionPanel
          expanded={expandAddPolicy}
          onChange={(e, exp) => setExpandAddPolicy(!expandAddPolicy)}
        >
          <ExpansionPanelSummary
            expandIcon={<ExpandMoreIcon />}
            aria-controls="panel1a-content"
            id="panel1a-header"
          >
            <Typography variant="h6" style={{ textTransform: 'capitalize' }}>
              {pageType} Page
            </Typography>
          </ExpansionPanelSummary>
          <ExpansionPanelDetails>
            <DynamicPageFormFields
              history={props.history}
              pageType={pageType}
              setPageType={setPageType}
              formValues={pagesState.page}
              cancelClick={() => props.history.push('/pages')}
            />
          </ExpansionPanelDetails>
        </ExpansionPanel>
      </Grid>
    </Grid>
  );
};

export default DynamicPageAdd;
