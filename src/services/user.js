import API from '../utils/api';
import LocalStorageService from './localStorage';
const localStorageService = LocalStorageService.getService();

export const getUserList = (url) => {
    return API.get(`users`, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};

export const getUser = (id) => {
    return API.get(`users/${id}`, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};  

export const addUser = (data) => {
    return API.post(`users`, data, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};
  
export const updateUser = (data) => {
    return API.put(`users/${data._id}`, data, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};

export const deleteUser = (data) => {
    return API.post(`users/delete`, {'ids': data}, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};