import API from '../utils/api';
import LocalStorageService from './localStorage';
const localStorageService = LocalStorageService.getService();

export const getPagesList = (url) => {
    return API.get(`pages`, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};

export const getPages = (id) => {
    return API.get(`pages/${id}`, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};  

export const addPages = (data) => {
    return API.post(`pages`, data, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};
  
export const updatePages = (data) => {
    return API.put(`pages/${data._id}`, data, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};

export const deletePages = (data) => {
    return API.post(`pages/delete`, {'ids': data}, {
        headers: {
            authorization: `${LocalStorageService.getAccessToken()}`,
        },
    });
};